import { logStep } from "../utils/reporter";
import { ChainablePromiseElement } from 'webdriverio'

export default class ConfirmationPage {


    get confirmationSentence() { return $("span[id*='_Sentence']") }
    get orderStatus() {return $("span[id*='STATUS']")}
    get orderNumberLink() {return $("a[id*='Link']")}

}