export class ShopDevicePageObjects {
    get shopButton() { return $$("div[id='scrollArea'] a span b") }
    get activityTimeout() { return $$("div[id='CRMMessageLine1'] span") }
    get productTypeDropdown() { return $("input[id*='search_zprodtype']") }
    get productTypeDropdownItems() { return $$("div[id*='search_zprodtype__items'] a") }
    get osTypeDropdown() { return $("input[id*='search_zos']") }
    get osTypeDropdownItems() { return $$("div[id*='search_zos__items'] a") }

    get deviceTable() { return $("table[id*='ConfCellTable_TableHeader']") }
    getDeviceCheckBox(deviceName: string) {
        return $("//a[contains(text(),'" + deviceName + "')]//ancestor::tr[1]//a[@class='th-sapcb-a']");
    }

    clickAddToCartButtonDevice(atIndex: number) {
        return $("table[id*='ConfCellTable_TableHeader'] tbody tr:nth-child(" + atIndex + ") td[class*='last-col'] a")
    }

}
