

export class ProfileOverviewPage {
    get profilePOverviewTable() { return $("table[id*='ConfCellTable_TableHeader']") }
    get closeMessageArea() { return $("div[id='th-footerMessAContainer'] a[title*='Collapse']") }
    get financialAccount() { return $("table[id*='ConfCellTable_TableHeader'] tbody tr:nth-child(1) td:nth-child(2) a") }
    // get financialAccount() {return $("//table[contains(@id,'ConfCellTable_TableHeader')]//tbody//tr[1]//td[2]//a")}
    //table[contains(@id,'ConfCellTable_TableHeader')]//tbody//tr[1]//td[2]
    get defaultPlan() { return $("table[id*='ConfCellTable_TableHeader'] tbody tr:nth-child(1) td:nth-child(1) a") }
    get clickMoreMenuDots() { return $("div.th-l-tb-hasovf a[id*='thtmlb_menuButton']") }
    get moreMenuOptions() { return $$("div.th-dym-listcontainer li a span.th-dym-text") }
    //


}