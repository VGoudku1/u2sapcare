import { logStep } from "../utils/reporter";
import { ChainablePromiseElement } from 'webdriverio'

export default class ActivationPage {


    get iMEICode() { return $("//label[contains(text(),'Zip Code')]//ancestor::td[@class='ch-grid-cell']//following-sibling::td//input") };
    get getOkButton() { return $("table.th-gr-tab a span b") }
    get errorMessageContainer() { return $("div[id='th-mess-cont']") }

    getTextBox(textBoxLabel: string) {
        return $("//label[contains(text(),'" + textBoxLabel + "')]//ancestor::td[@class='ch-grid-cell']//following-sibling::td//input");
    }

    getRadiotextBox(textBoxLabel: string) {
        return $("//label//span[contains(text(),'" + textBoxLabel + "')]//ancestor::td[@class='ch-grid-cell']//following-sibling::td//input");
    }

    getSimPlanAddToCartButton(simName: string) {
        return $("//span[contains(text(),'" + simName + "')]//parent::td//parent::tr//td//a[@title='Add to Cart']")
    }
}

////label//span[contains(text(),'')]//ancestor::td[@class='ch-grid-cell']//following-sibling::td//input