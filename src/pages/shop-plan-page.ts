
export class ShopPlanPageObjects {
    shopButton(planName: string) { return $("//span[contains(text(),'" + planName + "')]") }

    getPlanCheckBox(planName: string) {
        return $("//span[contains(text(),'" + planName + "')]//ancestor::tr[1]//a[@class='th-sapcb-a']");
    }

    clickAddToCartButton(atIndex: number) {
        // return $("//table[contains(@id,'ConfCellTable_TableHeader_fixedLeftTable')]//tbody//tr["+atIndex+"]//td[contains(@class,'first')]")
        return $("table[id*='ConfCellTable_TableHeader'] tbody tr:nth-child(" + atIndex + ") td[class*='first']")
    }

    get planTable() { return $("table[id*='ConfCellTable_TableHeader']") }
    get addToCartTable() { return $("table[id*='ConfCellTable_TableHeader_fixedLeftTable']") }

}