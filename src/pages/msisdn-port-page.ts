import { logStep } from "../utils/reporter";
import { ChainablePromiseElement } from 'webdriverio'

export default class MsISDNPage {


    get validAddressRadio() { return $("td input[type='radio'] ~label") };
    get acceptButton() { return $("li a[id*='CONFIRM'] span b") }
    get primaryNumberFirstCell() { return $("td[id*='ConfCellTable_sel_1']") }
    get secondaryNumberSecondCell() { return $("td[id*='ConfCellTable_sel_2']") }
    get phoneNoInResult() { return $("table[id*='ConfCellTable_TableHeader'] tbody tr td span[id*='result']") }
    get secondPhoneNoInResult() { return $("table[id*='ConfCellTable_TableHeader'] tbody tr td span[id*='result']") }

    getCustomerInfoTextBox(textBoxLabel: string) {
        return $("//label[contains(text(),'" + textBoxLabel + "')]//ancestor::td[@class='ch-grid-cell']//following-sibling::td//input[@value]");
    }

    getRadiotextBox(textBoxLabel: string) {
        return $("//label//span[contains(text(),'" + textBoxLabel + "')]//ancestor::td[@class='ch-grid-cell']//following-sibling::td//input");
    }
}

//label[contains(text(),'')]//ancestor::td[@class='ch-grid-cell']//following-sibling::td//input[@value]