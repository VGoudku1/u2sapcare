export class ManageRatePlanPage {
    get existinPlanTable() {return $("table[id*='ConfCellTable_TableHeader']")}
    get existingPlanFirstCell() {return $("td[id*='ConfCellTable_sel_1']")}
    get dollar50replaceButton() {return $("(//div[contains(@id,'ConfCellTable_bottom_div')]//table/tbody/tr/td//a//span//b[text()='Replace'])[3]")}
    get replacePlanTable() {return $("table[id*='thtmlb_grid_5'] tr:nth-child(5) table[id*='ConfCellTable_TableHeader']")}
    get replacePlanTable1() {return $("//p[text()='Search Plan Result']//parent::div//parent::div[contains(@id,'ConfCellTable_head')]//following-sibling::div[contains(@id,'ConfCellTable')]//table")}


    //p[text()='Search Plan Result']//parent::div//parent::div[contains(@id,'ConfCellTable_head')]//following-sibling::div[contains(@id,'ConfCellTable')]//table
    

}