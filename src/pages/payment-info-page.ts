

export class PaymentInfoPage {
    get footerBarButtons() { return $$("div[id='th-l-footerRoot'] ul span b"); }
    get editPaymentButton() { return $("table[id*=thtmlb_grid_1] tr a[id*='EDIT']") }
    get paymentMethodInput() { return $("input[id*='cnpmntmeth_pay_method']"); }
    get paymentOption() { return $$("div[id*='cnpmntmeth_pay_method__items'] a") }
    get paymentListBox() { return $("div.th-hb-ct[id*='cnpmntmeth_pay_method__items']") }
    get creditCardMonth() { return $("div.th-hb-ct[id*='cncreditdetail_card_exp_mon__items'] ~ div a") }
    get creditCardMonthListItems() { return $$("div.th-hb-ct[id*='cncreditdetail_card_exp_mon__items'] ul li a") }
    get creditCardYearList() { return $("div.th-hb-ct[id*='cncreditdetail_card_exp_year__items'] ~ div a") }
    get creditCardYearListItems() { return $$("div.th-hb-ct[id*='cncreditdetail_card_exp_year__items'] ul li a") }
    get consentCheckBox() { return $("a.th-sapcb-a[id*='check2']") }
    get validateAddressButton() {return $("//a[contains(@id,'button')]//span//b[text()='Validate']")}
    // get reviewTnCButton() {return $("//li//a[contains(@id,'REVTC')]//span//b[text()='Review T&C']")}
    get reviewTnCButton() {return $("(//span//b[text()='Review T&C'])[2]")}

}