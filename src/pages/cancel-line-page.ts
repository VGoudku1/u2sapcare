
export class CancelLinePage {
    get cancelReasonListBox() { return $("[role='listbox'] input[id*='terminfr_zz_termin']") }
    get reasonItems() { return $$("div[id*='terminfr_zz_termin__items'] li a") };
    get todayRadio() { return $("label[for*='CURRENTDATE'] span") }

}