import { WebActions } from "../utils/WebActions";
import { HomePageObjects } from "../pages/home-page-obj";
import { BrowserUtils } from "../utils/BrowserUtils";
import ActivationPage from "../pages/activation-page";
import { IfrmaeObjects } from "../pages/iframes-page";
import { ShopDeviceStepsCode } from "./shop-device-code";
import { ShopDevicePageObjects } from "../pages/shop-devices-page";
import { WaitUtils } from "../utils/WaitUtils";
import ConfirmationPage from "../pages/confirmation-page";
import { logStep } from "../utils/reporter";
import { Common } from "../utils/CommonEnum";
import { FileUtils } from "../utils/fileutils";

const homePage = new HomePageObjects();
const browserUtil = new BrowserUtils();
const activationPage = new ActivationPage();
const iFrames = new IfrmaeObjects();
const shopDeviceStepsCode = new ShopDeviceStepsCode();
const shopDevice = new ShopDevicePageObjects();
const waitUtils = new WaitUtils();
const confirmationPage = new ConfirmationPage();
const common = new Common();
const fileUtils = new FileUtils();

let orderNumberFile = "test-data/order-number.txt";

export class ConfirmationStepsCode extends WebActions {

    async verifyOrderGenerated() {
        let orderNumber: string = await this.captureOrderNumber();
        let orderText = "Your order " + orderNumber + " has been processed"
        expect(await this.fetchText(confirmationPage.confirmationSentence)).toContain(orderText);
    }

    async captureOrderNumber() {
        const orderNumber = await this.fetchText(confirmationPage.orderNumberLink);
        logStep(orderNumber)
        return orderNumber;
    }

    async writeOrderNumberToFile(orderNumber) {
        fileUtils.writeToFile(orderNumberFile, orderNumber)
    }
}