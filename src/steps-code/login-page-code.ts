import BasePage from "../pages/base-page";
import { HomePageObjects } from "../pages/home-page-obj";
import { IfrmaeObjects } from "../pages/iframes-page";
import loginPage from "../pages/login-page-obj";
import { UserData } from "../user-data/users";
import { BrowserUtils } from "../utils/BrowserUtils";
import { WebActions } from "../utils/WebActions";
const sapCareUrl: string = "https://ql2a01-esaw2a.lab.uprising.t-mobile.com:8081/sap(bD1lbiZjPTIwMCZkPW1pbg==)/bc/bsp/sap/crm_ui_start/default.htm?saprole=ZTMUS_SUPCAR&sap-client=200&sap-language=EN";
const basePage = new BasePage();
const webActions: WebActions = new WebActions();
const browserUtils = new BrowserUtils();
const homePage = new HomePageObjects();
const iFrames = new IfrmaeObjects();

class LoginStepsCode extends WebActions {

    async openApplication() {
        await basePage.open(sapCareUrl);
        // await super.open("http://automationpractice.com/index.php")
    }

    async clickOnSingIn() {
        await this.clickElement(loginPage.signInBtn);
    }

    async loginPerSession() {
        const userName = await this.isElementDisplayed(loginPage.userName);
        console.info("Login in new session !! ", userName)
        if (userName) {
            await this.enterUserName()
            await browser.pause(2000);
            await this.clickOnSingIn()
            await browser.pause(1000);
            await this.killExistingSession(false);
            await this.sessionContinueButton()
            await browser.pause(15000);
        } else {
            console.info("Login in existing session !!");
            await browserUtils.switchFrameByLocator(iFrames.iFrame1);
            await browserUtils.switchFrameByLocator(iFrames.Frame1);
            await this.clickElement(homePage.endSessionButton);
            await browser.pause(15000);
        }

    }

    async enterUserName() {
        await this.setData(UserData.USERS.TEST_TEAM.userName, loginPage.userName)
        await this.setData(UserData.USERS.TEST_TEAM.password, loginPage.password)
    }

    async killExistingSession(kill: boolean) {
        if (kill) {
            console.info("Existing Sessions Killed!!");
        } else {
            console.info("Existing Sessions Not Killed!!");
            await this.clickElement(loginPage.killSessionCheckBox);
        }



    }

    async sessionContinueButton() {
        await this.clickElement(loginPage.sessionContinue);
    }

}
export default new LoginStepsCode()