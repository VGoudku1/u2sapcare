import { WebActions } from "../utils/WebActions";
import { ReviewCartPage } from "../pages/review-cart-page";
import { WaitUtils } from "../utils/WaitUtils";
import { TableUtils } from "../utils/tableUtils";
import { SearchOrdersPage } from "../pages/search-orders-page";

const reviewCartPage = new ReviewCartPage();
const searchOrdersPage = new SearchOrdersPage
const waitUtils = new WaitUtils();

export class SearchOrdersStepsCode extends WebActions {

    async clickCustomerID(orderNumber: string) {
        const table: TableUtils = new TableUtils(searchOrdersPage.searchTables);
        await table.clickOnCellLinkWithText(orderNumber, 1, 5)
    }

}