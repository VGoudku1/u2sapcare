import { WebActions } from "../utils/WebActions";
import { ReviewCartPage } from "../pages/review-cart-page";
import { WaitUtils } from "../utils/WaitUtils";
const reviewCartPage = new ReviewCartPage();

const waitUtils = new WaitUtils();
export class ReviewCartStepsCode extends WebActions {

    async clickCartButtonWithText(buttonText: string) {
        await this.clickElementWithText(buttonText, await reviewCartPage.cartButtons);
    }

    async selectMoreOptions(optionName: string) {
        await this.clickElementWithText(optionName, await reviewCartPage.moreOptions);
    }

}