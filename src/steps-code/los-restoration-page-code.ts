import { WebActions } from "../utils/WebActions";
import { ReviewCartPage } from "../pages/review-cart-page";
import { WaitUtils } from "../utils/WaitUtils";
import { TableUtils } from "../utils/tableUtils";
import { ProfileOverviewPage } from "../pages/profile-overview-page";
import { SuspendLinePage } from "../pages/suspend-line-page";
import { LosRestorationPage } from "../pages/los-restoration-process-page";

const reviewCartPage = new ReviewCartPage();
const profileOverview = new ProfileOverviewPage()
const waitUtils = new WaitUtils();
const losRestoreationPage = new LosRestorationPage();

export class LosRestorationStepsCode extends WebActions {

    async selectReasonToRestore(reason: string) {
        await waitUtils.waitElementToVisible(losRestoreationPage.reasontoRestore, 5);
        await this.selectByText(reason, losRestoreationPage.reasontoRestore);
    }

}