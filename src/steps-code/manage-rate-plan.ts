import { WebActions } from "../utils/WebActions";
import { WaitUtils } from "../utils/WaitUtils";
import { ManageRatePlanPage } from "../pages/manage-rate-plan-page";
import { BrowserUtils } from "../utils/browserUtils";
import { IfrmaeObjects } from "../pages/iframes-page";
import { ShopPlanStepsCode } from "./shop-plan-code";
import { ShopPlanPageObjects } from "../pages/shop-plan-page";
import { TableUtils } from "../utils/tableUtils";

const waitUtils = new WaitUtils();
const manageRatePlanPage = new ManageRatePlanPage();
const browserUtil = new BrowserUtils();
const iFrame = new IfrmaeObjects();
const webActions = new WebActions();
const shopPlan = new ShopPlanPageObjects();

export class ManageRatePlanStepsCode {

    async selectExistingPlanRow(planName: string) {
        await browserUtil.switchAllFrames(true)
        await webActions.clickElement(manageRatePlanPage.existingPlanFirstCell);
    }

    async selectPlanToReplace(planName: string, iFrame: boolean) {
        await webActions.clickElement(shopPlan.getPlanCheckBox(planName));
    }

    async clickReplaceButton(planName: string) {
        const tableUtils = new TableUtils(manageRatePlanPage.replacePlanTable);
        await tableUtils.clickOnCellLinkWithText(planName,7,10);
        // await webActions.clickElement(manageRatePlanPage.dollar50replaceButton);
        await browser.pause(4000);
    }

}