import { WebActions } from "../utils/WebActions";
import { HomePageObjects } from "../pages/home-page-obj";
import { BrowserUtils } from "../utils/BrowserUtils";
import ActivationPage from "../pages/activation-page";
import { IfrmaeObjects } from "../pages/iframes-page";
import { ShopDeviceStepsCode } from "./shop-device-code";
import { FileUtils } from "../utils/fileutils";
import MsISDNPage from "../pages/msisdn-port-page";

const homePage = new HomePageObjects();
const browserUtil = new BrowserUtils();
const activationPage = new ActivationPage();
const iFrames = new IfrmaeObjects();
const shopDeviceStepsCode = new ShopDeviceStepsCode();
const msIsdnPage = new MsISDNPage();
const webActions = new WebActions();
const fileUtils = new FileUtils();

let orderNumberFile = "test-data/order-number.txt";
export class MsISDNPortStepsCode extends WebActions {

    async enterSearchNPAMobileNo(NPA: string) {
        await this.setData(NPA, msIsdnPage.getRadiotextBox('NPA'));
    }

    async setDataInTextBox(textBoxLabel: string, dataToSet: string) {
        await this.setData(dataToSet, msIsdnPage.getCustomerInfoTextBox(textBoxLabel));
    }

    async clickFirstPhoneNo() {
        await this.clickElement(msIsdnPage.phoneNoInResult);
    }

    async selectPrimaryNumber() {
        await webActions.clickElement(msIsdnPage.primaryNumberFirstCell);
    }

    async selectSecondaryNumber() {
        await webActions.clickElement(msIsdnPage.secondaryNumberSecondCell);
    }

    async readOrderNumberFromFile() {
        const orderNumber = fileUtils.readFromFile(orderNumberFile);
        return orderNumber
    }


}