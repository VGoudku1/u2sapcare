import { WebActions } from "../utils/WebActions";
import { HomePageObjects } from "../pages/home-page-obj";
import { BrowserUtils } from "../utils/BrowserUtils";
import ActivationPage from "../pages/activation-page";
import { IfrmaeObjects } from "../pages/iframes-page";
import { ShopDeviceStepsCode } from "./shop-device-code";
import { ShopDevicePageObjects } from "../pages/shop-devices-page";
import { WaitUtils } from "../utils/WaitUtils";
import { logStep } from "../utils/reporter";

const homePage = new HomePageObjects();
const browserUtil = new BrowserUtils();
const activationPage = new ActivationPage();
const iFrames = new IfrmaeObjects();
const shopDeviceStepsCode = new ShopDeviceStepsCode();
const shopDevice = new ShopDevicePageObjects();
const waitUtils = new WaitUtils();
export class ActivationStepsCode extends WebActions {

    async enterZipCode(zipCode: string) {
        await this.setData(zipCode, activationPage.getTextBox('Zip Code'));
    }

    async enterSimNo(simNo: string) {
        await this.setData(simNo, activationPage.getTextBox('SIM'));
    }

    async enterIMEICode(imeiCode: string) {
        await this.setData(imeiCode, activationPage.getRadiotextBox('IMEI'));
    }

    async handleActivityTimeOut() {
        await browser.pause(2000);
        let errorArray = [];
        let noOfElements: number = (await shopDevice.activityTimeout).length;

        for (var i = 0; i <= noOfElements - 1; i++) {
            let errorText = await (await shopDevice.activityTimeout[i]).getText();
            errorArray.push(errorText.toLowerCase());
        }
        console.log("The Array of Errors: ", errorArray)
        if (errorArray.includes('new sim number entered is already reserved!!')) {
            Promise.reject("Sim is already reserved.")
        } else if (errorArray.includes('new sim number entered is already inuse!!')) {
            Promise.reject("Sim number entered is already inuse")
        } else if (errorArray.includes('blacklist')) {
            Promise.reject("imei is blacklisted.you can continue shopping device or sim")
        }
        else if (errorArray.includes('activity timed out:interface error')) {
            logStep("Validate Activity Timed Out Trying again to Validate.")
            await this.clickElementWithText('VALIDATE', await shopDevice.shopButton);
            await browser.pause(3000);
            const browsersOpened = await browserUtil.getOpenedBrowsers();
            if (browsersOpened.length > 1) {
                await browserUtil.switchToWindow(2);
                await browser.closeWindow();
                console.log("Switching Back to browser 1")
                await browserUtil.switchToWindow(1);
                await browser.pause(3000);
            }
        }

    }

    async messageContainer() {
        const messgeContainer = await this.isElementDisplayed(activationPage.errorMessageContainer);
        if (messgeContainer) {
            console.log("Message Container Displayed Handling it")
            await this.handleActivityTimeOut();
            if (messgeContainer) {
                console.log("Message Container Still Displayed1")
                await this.handleActivityTimeOut();
            }
        } else {
            await this.clickOkValidationPopup();
            const messgeContainer1 = await this.isElementDisplayed($("div[id='th-mess-cont1']"));
            if (messgeContainer1) {
                console.log("Message Container Still Displayed2")
                await this.handleActivityTimeOut();
            }
        }
    }

    async clickOkValidationPopup() {
        await browser.pause(3000);
        const browsersOpened = await browserUtil.getOpenedBrowsers();
        if (browsersOpened.length > 1) {
            await browserUtil.switchToWindow(2);
            await browser.closeWindow();
            console.log("Switching Back to browser 1")
            await browserUtil.switchToWindow(1);
            await browser.pause(3000);
        } else {
            await this.handleActivityTimeOut();
        }
    }

    async clickAddToCartSIM(simName: string) {
        await this.clickElement(activationPage.getSimPlanAddToCartButton(simName));
    }
}