import { WebActions } from "../utils/WebActions";
import { ReviewCartPage } from "../pages/review-cart-page";
import { WaitUtils } from "../utils/WaitUtils";
import { ProfileOverviewPage } from "../pages/profile-overview-page";
import { CancelLinePage } from "../pages/cancel-line-page";

const reviewCartPage = new ReviewCartPage();
const profileOverview = new ProfileOverviewPage()
const waitUtils = new WaitUtils();
const cancelLinePage = new CancelLinePage();

export class CancelLineStepsCode extends WebActions {

    async selectReasonToCancel(reason: string) {
        await waitUtils.waitElementToVisible(cancelLinePage.cancelReasonListBox, 5);
        await this.scrollIntoView(cancelLinePage.cancelReasonListBox);
        await this.clickElement(cancelLinePage.cancelReasonListBox);
        await this.clickElementWithText(reason, await cancelLinePage.reasonItems)
    }

    async selectTodayRadioButton() {
        await waitUtils.waitElementToVisible(cancelLinePage.todayRadio, 5);
        await this.clickElement(cancelLinePage.todayRadio);
    }


    
}