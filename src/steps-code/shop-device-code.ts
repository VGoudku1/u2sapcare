import { WebActions } from "../utils/WebActions";
import { HomePageObjects } from "../pages/home-page-obj";
import { ShopDevicePageObjects } from "../pages/shop-devices-page";
import { BrowserUtils } from "../utils/browserUtils";
import { IfrmaeObjects } from "../pages/iframes-page";
import { WaitUtils } from "../utils/WaitUtils";
import { logStep } from "../utils/reporter";
import { TableUtils } from "../utils/tableUtils";
import { ShopPlanPageObjects } from "../pages/shop-plan-page";

const iFrames = new IfrmaeObjects();
const browserUtil = new BrowserUtils();
const shopDevice = new ShopDevicePageObjects();
const wait = new WaitUtils();
const shopPlan = new ShopPlanPageObjects();
export class ShopDeviceStepsCode extends WebActions {

    async clickShopDeviceButton(buttonText: string) {
        await browser.pause(1000)
        await this.clickElementWithText(buttonText, await shopDevice.shopButton);
    }

    async clickiFrameButton(buttonText: string) {
        await browserUtil.switchAllFrames(true);
        await this.clickElementWithText(buttonText, await shopDevice.shopButton);
    }

    async selectFromProductTypeDropdown(productType: string) {
        await this.clickElement(shopDevice.productTypeDropdown);
        await browser.pause(2000);
        await this.clickElementWithText(productType, await shopDevice.productTypeDropdownItems);
    }

    async selectFromOsTypeDropdown(osType: string) {
        await this.clickElement(shopDevice.osTypeDropdown);
        await browser.pause(2000);
        await this.clickElementWithText(osType, await shopDevice.osTypeDropdownItems);
    }

    async selectDevice(deviceName: string, iFrame: boolean) {
        await browserUtil.switchAllFrames(iFrame)
        await this.clickElement(shopDevice.getDeviceCheckBox(deviceName));
    }

    async addToCartSelectedDevice(deviceName: string) {
        const tableUtils = new TableUtils(shopDevice.deviceTable);
        let textInRow: number = await tableUtils.getRowNumberContainingText(deviceName, 5);
        await (await shopDevice.clickAddToCartButtonDevice(textInRow)).scrollIntoView();
        (await shopDevice.clickAddToCartButtonDevice(textInRow)).moveTo();
        (await shopDevice.clickAddToCartButtonDevice(textInRow)).click();
        await browser.pause(5000);
    }

}