import { WebActions } from "../utils/WebActions";
import { ShopPlanPageObjects } from "../pages/shop-plan-page";
import { TableUtils } from "../utils/tableUtils";
import { BrowserUtils } from "../utils/browserUtils";
import { IfrmaeObjects } from "../pages/iframes-page";
import { ShopServicesPageObjects } from "../pages/shop-services-page";

const iFrames = new IfrmaeObjects();
const browserUtil = new BrowserUtils();
const shopServicePage = new ShopServicesPageObjects();

export class ShoServicesStepsCode extends WebActions {

    async selectServicePlan(planName: string, iFrame: boolean) {
        await browserUtil.switchAllFrames(iFrame)
        // await browserUtil.validateFrameElement();
        // await this.clickElement(shopPlan.getPlanCheckBox(planName));
    }

    async clickAddToCartButtonForAddOnService(serviceName: string) {
        const tableUtils = new TableUtils(shopServicePage.servicePlanTable);
        await tableUtils.clickOnCellLinkWithText(serviceName, 3, 7);
    }
}