import { WebActions } from "../utils/WebActions";
import { ShopPlanPageObjects } from "../pages/shop-plan-page";
import { TableUtils } from "../utils/tableUtils";
import { BrowserUtils } from "../utils/browserUtils";
import { IfrmaeObjects } from "../pages/iframes-page";
import { ShopDevicePageObjects } from "../pages/shop-devices-page";

const iFrames = new IfrmaeObjects();
const browserUtil = new BrowserUtils();
const shopPlan = new ShopPlanPageObjects();
const shopDevice = new ShopDevicePageObjects();

export class ShopPlanStepsCode extends WebActions {

    async selectPlan(planName: string, iFrame: boolean) {
        await browserUtil.switchAllFrames(iFrame)
        await this.clickElement(shopPlan.getPlanCheckBox(planName));
    }

    async clickAddToCartButton(planName: string) {
        const tableUtils = new TableUtils(shopPlan.planTable);
        let textInRow: number = await tableUtils.getRowNumberContainingText(planName, 3);
        await (await shopPlan.clickAddToCartButton(textInRow)).scrollIntoView();
        (await shopPlan.clickAddToCartButton(textInRow)).moveTo();
        (await shopPlan.clickAddToCartButton(textInRow)).click();
        await browser.pause(5000);
    }

    async clickAddToCartButtonForDevicePlan(planName: string) {
        const tableUtils = new TableUtils(shopPlan.planTable);
        let textInRow: number = await tableUtils.getRowNumberContainingText(planName, 6);
        await (await shopDevice.clickAddToCartButtonDevice(textInRow)).scrollIntoView();
        await (await shopDevice.clickAddToCartButtonDevice(textInRow)).click();
        await browser.pause(5000);
    }
}