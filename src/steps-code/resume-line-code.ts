import { WebActions } from "../utils/WebActions";
import { ReviewCartPage } from "../pages/review-cart-page";
import { WaitUtils } from "../utils/WaitUtils";
import { ResumeLinePage } from "../pages/resume-line-page";

const waitUtils = new WaitUtils();
const resumeLinePage = new ResumeLinePage();

export class ResumeLineStepsCode extends WebActions {

    async selectReasonToResume(reason: string) {
        await waitUtils.waitElementToVisible(resumeLinePage.reasonToResume, 5);
        await this.selectByText(reason, resumeLinePage.reasonToResume);
    }

}