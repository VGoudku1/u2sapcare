import { WebActions } from "../utils/WebActions";
import { ReviewCartPage } from "../pages/review-cart-page";
import { WaitUtils } from "../utils/WaitUtils";
import { TableUtils } from "../utils/tableUtils";
import { ProfileOverviewPage } from "../pages/profile-overview-page";
import { SuspendLinePage } from "../pages/suspend-line-page";

const reviewCartPage = new ReviewCartPage();
const profileOverview = new ProfileOverviewPage()
const waitUtils = new WaitUtils();
const suspenLinePage = new SuspendLinePage();

export class SuspendLineStepsCode extends WebActions {

    async selectReasonToSuspend(reason: string) {
        await waitUtils.waitElementToVisible(suspenLinePage.reasontoSuspend, 5);
        await this.selectByText(reason, suspenLinePage.reasontoSuspend);
    }

}