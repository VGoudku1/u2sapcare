import { WebActions } from "../utils/WebActions";
import { ReviewCartPage } from "../pages/review-cart-page";
import { WaitUtils } from "../utils/WaitUtils";
import { TableUtils } from "../utils/tableUtils";
import { ProfileOverviewPage } from "../pages/profile-overview-page";
import { BrowserUtils } from "../utils/browserUtils";
import { IfrmaeObjects } from "../pages/iframes-page";

const reviewCartPage = new ReviewCartPage();
const profileOverview = new ProfileOverviewPage()
const waitUtils = new WaitUtils();
const browserUtils = new BrowserUtils();
const iFrame = new IfrmaeObjects();

export class ProfileOverviewStepsCode extends WebActions {

    async clickFinancialAccount() {
        await this.clickElementIfExists(profileOverview.closeMessageArea)
        await waitUtils.waitElementToClickable(profileOverview.financialAccount, 5)
        await this.clickElement(profileOverview.financialAccount);
        await browser.pause(2000);
    }

    async selectPlanCheckBox() {
        await waitUtils.waitElementToClickable(profileOverview.defaultPlan, 5)
        await this.clickElement(profileOverview.defaultPlan);
    }

    async selectPlanToSuspend(planName: string) {
        const table: TableUtils = new TableUtils(profileOverview.profilePOverviewTable);
        await table.clickOnCellLinkWithText(planName, 3, 1)
    }

    async selectMoreMenuOption(optionName: string) {
        await waitUtils.waitElementToClickable(profileOverview.clickMoreMenuDots, 5)
        await this.clickElement(profileOverview.clickMoreMenuDots);
        await browser.pause(2000);
        await this.clickElementWithText(optionName, await profileOverview.moreMenuOptions);
        
    }

}