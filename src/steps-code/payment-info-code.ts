import { WebActions } from "../utils/WebActions";
import { HomePageObjects } from "../pages/home-page-obj";
import { BrowserUtils } from "../utils/browserUtils";
import ActivationPage from "../pages/activation-page";
import { IfrmaeObjects } from "../pages/iframes-page";
import { PaymentInfoPage } from "../pages/payment-info-page";

import CustomerInfoPage from "../pages/customer-info-page";
import MsISDNPage from "../pages/msisdn-port-page";
import { WaitUtils } from "../utils/WaitUtils";

const homePage = new HomePageObjects();
const browserUtil = new BrowserUtils();
const customerInfoPage = new CustomerInfoPage();
const iFrames = new IfrmaeObjects();
const waitUtils = new WaitUtils();

const paymentInfoPage = new PaymentInfoPage();
export class PaymentInfoStepsCode extends WebActions {

    async clickFooterBarButton(buttonText: string) {
        await browser.pause(3000);
        await this.clickElementWithText(buttonText, await paymentInfoPage.footerBarButtons);
    }

    async clickEditPaymentButton() {
        await browser.pause(3000);
        await this.clickElement(paymentInfoPage.editPaymentButton);
        await browser.pause(8000);
    }

    async selectPaymentMethod(paymentOption: string) {
        await (await paymentInfoPage.paymentMethodInput).scrollIntoView();
        await this.clickElement(paymentInfoPage.paymentMethodInput);
        await browser.pause(1000)
        await this.clickElementWithText(paymentOption, await paymentInfoPage.paymentOption);
        await browser.pause(8000)
    }

    async fillCreditCardDetails() {
        await this.setData("CreditF", customerInfoPage.getCustomerInfoTextBox('First Name'));
        await this.setData("CreditL", customerInfoPage.getCustomerInfoTextBox('Last Name'));
        await this.clickElement(paymentInfoPage.creditCardMonth);
        await browser.pause(2000)
        await this.clickElementWithText('June', await paymentInfoPage.creditCardMonthListItems);
        await this.clickElement(paymentInfoPage.creditCardYearList);
        await browser.pause(1000)
        await this.clickElementWithText("2024", await paymentInfoPage.creditCardYearListItems);
        // await this.selectItemInList('May', paymentInfoPage.creditCardMonthListBox);
        // await this.selectItemInList('2024', paymentInfoPage.creditCardYearListBox);
        await this.setData("349956153891398", customerInfoPage.getCustomerInfoTextBox('Card Number:'));
        await this.clickElement(customerInfoPage.getCustomerInfoTextBox('Last Name'));
        await browser.pause(5000)
        await this.setData("1111", customerInfoPage.getCustomerInfoTextBox('CVV'));
    }

    async clickConsentCheckBox() {
        await browserUtil.switchAllFrames(true);
        await this.scrollIntoView(paymentInfoPage.consentCheckBox);
        await this.clickElement(paymentInfoPage.consentCheckBox);
    }

    async clickSuspendConsentCheckBox() {
        await this.scrollIntoView(paymentInfoPage.consentCheckBox);
        await this.clickElement(paymentInfoPage.consentCheckBox);
    }

    async validateAddress(){
        await this.clickElement(paymentInfoPage.validateAddressButton);
        await browser.pause(2000);
    }

    async reviewTermsAndConditionsButton(){
        await browserUtil.switchFrameByLocator(iFrames.iFrame5)
        await browserUtil.switchFrameByLocator(iFrames.iFrame4)
        await this.clickElement(paymentInfoPage.reviewTnCButton);
        await browser.pause(2000);
    }

}