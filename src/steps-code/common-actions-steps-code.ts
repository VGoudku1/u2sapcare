import { WebActions } from "../utils/WebActions";
import { HomePageObjects } from "../pages/home-page-obj";
import { BrowserUtils } from "../utils/browserUtils";
import ActivationPage from "../pages/activation-page";
import { IfrmaeObjects } from "../pages/iframes-page";
import { ShopDeviceStepsCode } from "./shop-device-code";
import { ShopDevicePageObjects } from "../pages/shop-devices-page";
import { WaitUtils } from "../utils/WaitUtils";

const homePage = new HomePageObjects();
const browserUtil = new BrowserUtils();
const activationPage = new ActivationPage();
const iFrames = new IfrmaeObjects();
const shopDeviceStepsCode = new ShopDeviceStepsCode();
const shopDevice = new ShopDevicePageObjects();
const waitUtils = new WaitUtils();
export class CommonStepsCode extends WebActions {

    async clickPopUpOkButton(){
        await browserUtil.popupButtonOk();
    }

}