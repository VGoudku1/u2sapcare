import fs from 'fs';
import { logStep } from './reporter';
import { join } from 'path'

let orderNumberFile = "./test-data/order-number.txt";
const srcDirPath = join(__dirname, '..')
export class FileUtils {

    public deleteDirectory(path: string) {
        if (fs.existsSync(path)) {
            fs.rmdirSync(path, { recursive: true })
            logStep(`Directory Deleted: ${path}`)
        }
    }

    public writeToFile(path: string, data: string) {
        const dirPath = join(srcDirPath, path)
        if (fs.existsSync(dirPath)) {
            fs.writeFileSync(dirPath, data)
            logStep(`Writing to file: ${path}`)
        }
    }

    public readFromFile(path: string) {
        const dirPath = join(srcDirPath, path)
        if (fs.existsSync(dirPath)) {
            const fileContent: string = fs.readFileSync(dirPath, 'utf-8')
            logStep(`Reading from file Deleted: ${dirPath}`)
            return fileContent
        }
    }

    public clearTextFile(path: string) {
        const dirPath = join(srcDirPath, path)
        if (fs.existsSync(dirPath)) {
            fs.rmdirSync(path, { recursive: true })
            logStep(`Directory Deleted: ${path}`)
        }
    }

    public searchTextInFileAndAppend(textToSearch: string, appendText: string, path: string) {
        const dirPath = join(srcDirPath, path)
        if (fs.existsSync(dirPath)) {
            const readLine = fs.readFileSync(dirPath, 'utf-8')
            let lineArray = readLine.split('/\r?\n/')
            console.log("File Array: ", lineArray)
            if (readLine.indexOf(textToSearch) >= 0) {
                readLine.concat(appendText)
            }

        }
    }

}