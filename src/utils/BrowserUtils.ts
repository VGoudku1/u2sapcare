import { logStep } from "./reporter";
import { ChainablePromiseElement } from 'webdriverio'
import { HomePageObjects } from "../pages/home-page-obj";
import { IfrmaeObjects } from "../pages/iframes-page";
import { WebActions } from "./WebActions";
import { WaitUtils } from "./WaitUtils";

const iFrames = new IfrmaeObjects();
const homePage = new HomePageObjects();
const waitUtils = new WaitUtils();
const webActions = new WebActions();

export class BrowserUtils {
    get getPopupButtonOK() { return $("li a[id*='btnOk'] span b") }
    async switchFrameByLocator(frame: ChainablePromiseElement<WebdriverIO.Element>) {
        const isFrameExists = (await (expect(await frame).toExist()));
        await browser.switchToFrame(await frame);
        logStep(`Switched Frame ${JSON.stringify(await frame.selector)}`);
    }

    async validateFrameElement() {
        console.log("Validating Frames")
        console.log("iFrames.iFrame1 is existing: ", await iFrames.iFrame1.isExisting())
        console.log("iFrames.iFrame2 is existing: ", await iFrames.iFrame2.isExisting())
        console.log("iFrames.iFrame3 is existing: ", await iFrames.iFrame3.isExisting())
        console.log("iFrames.iFrame4 is existing: ", await iFrames.iFrame4.isExisting())
        console.log("iFrames.iFrame5 is existing: ", await iFrames.iFrame5.isExisting())
        console.log("iFrames.Frame is existing: ", await iFrames.Frame.isExisting())
        console.log("iFrames.Frame1 is existing: ", await iFrames.Frame1.isExisting())
    }

    async switchAllFrames(switchAll: boolean) {
        if (switchAll) {
            await this.switchFrameByLocator(iFrames.iFrame1);
            await this.switchFrameByLocator(iFrames.iFrame2);
            await this.switchFrameByLocator(iFrames.iFrame1);
            await this.switchFrameByLocator(iFrames.iFrame3);
            await this.switchFrameByLocator(iFrames.Frame1);
        }
        logStep(`Switched All Frames`);
    }

    async excuteJSQuery(query: string) {
        await browser.execute(query);
    }

    async getOpenedBrowsers() {
        const handles = await browser.getWindowHandles();
        return handles;
    }

    async switchToWindow(windowNumber: number) {
        const switchTo = windowNumber - 1;
        const handles = await browser.getWindowHandles();
        console.log("No of browsers: ", handles.length)
        for (let i: number = 0; i <= handles.length - 1; i++) {
            if (switchTo == i) {
                await browser.switchToWindow(handles[i])
                console.log("Switched to Browser with Title: ", await browser.getTitle())
                break;
            }
        }
    }

    async popupButtonOk() {
        await browser.pause(3000);
        const browsersOpened = await this.getOpenedBrowsers();
        if (browsersOpened.length > 1) {
            await this.switchToWindow(2);
            await this.switchFrameByLocator(iFrames.popupFrame1);
            await waitUtils.waitElementToClickable(this.getPopupButtonOK, 5)
            await webActions.clickElement(this.getPopupButtonOK);
            await this.switchToWindow(1);
            await browser.pause(3000);
        } else {
            await Promise.reject("Validation Unscussessfull")
        }
    }
}