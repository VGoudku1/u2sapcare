export class Common {

    private ORDER_NUMBER
    constructor() {
        this.ORDER_NUMBER = new Map();
    }

    save<T>(key:number, value:string):void{
        this.ORDER_NUMBER.set(key, value);
    }

    load<T>(key:number):T{
        if (this.ORDER_NUMBER.has(key)){
            console.log("Map has key: ", this.ORDER_NUMBER.get(key))
            return this.ORDER_NUMBER.get(key)
        }
    }
}



