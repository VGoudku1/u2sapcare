import * as EC from 'wdio-wait-for'
import { logStep } from "../utils/reporter";
import { ChainablePromiseElement } from 'webdriverio'
import { WebActions } from './WebActions';
import { HomePageObjects } from '../pages/home-page-obj';

const ms = 1000
const webActions = new WebActions();
const homePage:HomePageObjects = new HomePageObjects();
export class WaitUtils {

    waitUntillCondition = async (condition: boolean, timeout: number, errormsg: string) => {
        await browser.waitUntil(async () => condition, { timeout: timeout, timeoutMsg: errormsg })
    }

    /**
     * Wait till ms seconds for element to be clickable
     * @param element - element expected to be clicked
     * @param waitTimems - wait for element in Mili Seconds
     */
    async waitElementToClickable(element: ChainablePromiseElement<WebdriverIO.Element>, waitTimems: number) {
        const elementCss: string = JSON.stringify(await element.selector);
        await element.waitUntil(EC.elementToBeClickable(element), { timeout: ms * waitTimems, timeoutMsg: 'Waited' + ms + 'seconds for' + `${elementCss}` + ' to be clickable' });
    }

    /**
     * Wait till ms seconds for element to be enabled
     * @param element - element expected to be enabled
     * @param waitTimems - wait for element in Mili Seconds
     */
    async waitElementToEnabled(element: ChainablePromiseElement<WebdriverIO.Element>, waitTimems: number) {
        const elementCss: string = JSON.stringify(await element.selector);
        await element.waitUntil(EC.elementToBeEnabled(element), { timeout: ms * waitTimems, timeoutMsg: 'Waited' + ms + 'seconds for' + `${elementCss}` + ' to be enabled' });
    }

    /**
     * Wait till ms seconds for element to be visible
     * @param element - element expected to be enabled
     * @param waitTimems - wait for element in Mili Seconds
     */
    async waitElementToVisible(element: ChainablePromiseElement<WebdriverIO.Element>, waitTimems: number) {
        const elementCss: string = JSON.stringify(await element.selector);
        await element.waitUntil(EC.visibilityOf(element), { timeout: ms * waitTimems, timeoutMsg: 'Waited' + ms + 'seconds for' + `${elementCss}` + ' to be enabled' });
    }

    /**
     * Wait till ms seconds for element to be visible
     * @param element - element expected to be enabled
     * @param waitTimems - wait for element in Mili Seconds
     */
    async waitElementToExist(element: ChainablePromiseElement<WebdriverIO.Element>, waitTimems: number) {
        const elementCss: string = JSON.stringify(await element.selector);
        await element.waitUntil(EC.presenceOf(element), { timeout: ms * waitTimems, timeoutMsg: 'Waited' + ms + 'seconds for' + `${elementCss}` + ' to be enabled' });
    }

    /**
    * Wait till ms seconds for element to be clickable
    * @param element - element expected to be clicked
    * @param waitTimems - wait for element in Mili Seconds
    */
    async waitForElementWithText(elementText: string, element: Array<WebdriverIO.Element>, waitTimems: number) {
        let ele = await webActions.getWebElementWithText(elementText, element)
        const elementCss: string = JSON.stringify(ele.selector);
        console.log("The element: ", elementCss)
        await ele.waitUntil(EC.textToBePresentInElement(elementCss, elementText), { timeout: ms * waitTimems, timeoutMsg: 'Waited' + ms + 'seconds for' + `${elementCss}` + ' to be clickable' });
    }

    /**
    * Wait till ms seconds for element to be clickable
    * @param element - element expected to be clicked
    * @param waitTimems - wait for element in Mili Seconds
    */
    async waitForSpinWheel(waitTimems: number) {
        // await browserUtils.switchAllFrames(true);
        let ele = await webActions.getWebElement(homePage.getSpinWheen())
        const isDisp = await ele.isDisplayed();
        console.log("SpinWheel Displyed: ", isDisp)
        const elementCss: string = JSON.stringify(ele.selector);
        console.log("The element: ", await EC.invisibilityOf(homePage.getSpinWheen()))
        await ele.waitUntil(EC.invisibilityOf(homePage.getSpinWheen()), { timeout: ms * waitTimems, timeoutMsg: 'Waited' + ms + 'seconds for' + `${elementCss}` + ' to be clickable' });
    }

}

