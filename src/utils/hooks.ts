import { ITestCaseHookParameter, Status } from '@cucumber/cucumber';
import { Before, After } from '@wdio/cucumber-framework';
import CucumberJsJsonReporter from 'wdio-cucumberjs-json-reporter';


Before(async function (scenario: ITestCaseHookParameter) {

})


After(async function (testCase: ITestCaseHookParameter) {
    if (testCase.result.status === Status.FAILED) {
        const takeScreenshot = await browser.takeScreenshot();
        const attachment = Buffer.from(takeScreenshot, 'base64');
        CucumberJsJsonReporter.attach(takeScreenshot, 'image/png');
    }

})