import * as EC from 'wdio-wait-for'
import { logStep } from "../utils/reporter";
import { ChainablePromiseElement } from 'webdriverio'
import { WebActions } from './WebActions';
import { forEachChild } from 'typescript';

export class TableUtils extends WebActions {

    nativeTableLocator: ChainablePromiseElement<WebdriverIO.Element>;
    constructor(tableLocator: ChainablePromiseElement<WebdriverIO.Element>) {
        super();
        this.nativeTableLocator = tableLocator;
    }

    /**
     * @returns total number of rows in table
     */
    async getRowCount() {
        const table = await this.nativeTableLocator;
        const tableRows = await (await table.$('tbody')).$$('tr');
        return tableRows.length;
    }

    /**
    * @returns total number of header columns in table
    */
    async getColumnsCount() {
        const table = await this.nativeTableLocator;
        const tableColumns = await (await table.$$('th'));
        return tableColumns.length;
    }

    /**
    * @param rowText - text to search in all rows of table
    * @param inColumn - text to search in column number table
    * @returns row number where text contains matches
    */
    async getRowNumberContainingText(rowText: string, inColumn: number) {
        let tableRowText: string = null;
        let rowNumber: number = null;
        const table = await this.nativeTableLocator;
        const noOfRows = await this.getRowCount();
        console.log("No of Rows: ", noOfRows);
        const row = await (await table.$('tbody')).$$('tr td:nth-child(' + inColumn + ')');
        let rows: number = row.length;
        if (rows > 1) {
            for (let i: number = 0; i <= noOfRows - 1; i++) {
                tableRowText = (await row[i].getText()).trim();
                if (tableRowText.toLowerCase().includes(rowText.toLowerCase())) {
                    rowNumber = i;
                    break;
                }
            }
        } else {
            tableRowText = await (await row[0].getText()).trim();
            if (tableRowText.includes(rowText.toLowerCase())) {
                rowNumber = 0;
            }
        }

        return rowNumber + 1;
    }

    /**
    * @param rowText - text to search in all rows of table
    * @param inColumn - text to search in column number table
    * @returns row number where text contains matches
    */
    async clickOnCellLinkWithText(rowText: string, findInColumn: number, clickInColumn: number) {
        let tableHeadText: string = null;
        let headerInColumn: number = null;
        let tableRowText: string = null;
        const table = await this.nativeTableLocator;
        const noOfRows = await this.getRowCount();
        console.log("No of Rows: ", noOfRows);
        const row = await (await table.$('tbody')).$$('tr td:nth-child(' + findInColumn + ')');
        if (noOfRows > 1) {
            for (let i: number = 0; i <= noOfRows - 1; i++) {
                tableRowText = (await row[i].getText()).trim();
                console.log("Table Row text: ", tableRowText)
                if (tableRowText.toLowerCase() == rowText.toLowerCase()) {
                    const rowElement = await (await table.$('tbody')).$('tr:nth-child(' + (i+1) + ') td:nth-child(' + clickInColumn + ') a');
                    console.log("Clicking on Cell: ", 'tr:nth-child(' + (i+1) + ') td:nth-child(' + clickInColumn + ') a')
                    console.log("Clicking on element: ", await rowElement.getText())
                    await rowElement.scrollIntoView()
                    await rowElement.click();
                    await browser.pause(3000)
                    break;
                }
            }
        } else {
            const rowElement = await (await table.$('tbody')).$('tr:nth-child(' + findInColumn + ') td:nth-child(' + clickInColumn + ') a');
            await rowElement.scrollIntoView()
            await rowElement.click();
            await browser.pause(3000)
        }
    }

    /**
    * @param rowText - text to search in all rows of table
    * @param inColumn - text to search in column number table
    * @returns row number where text contains matches
    */
    async clickOnRowIndexWithHeaderText(rowIndex: number, tableHeader: string) {
        let tableHeadText: string = null;
        let headerInColumn: number = null;
        let tableRowText: string = null;
        const table = await this.nativeTableLocator;
        const noOfRows = await this.getRowCount();
        console.log("No of Rows: ", noOfRows);
        const tHeads = await (await table.$('tbody thead')).$$('th');
        for (let i: number = 0; i <= tHeads.length - 1; i++) {
            tableHeadText = (await tHeads[i].getText()).trim();
            console.log("The Table Has Headers: ", tableHeadText)
            if (tableHeader.toLowerCase() == tableHeadText.toLowerCase()) {
                headerInColumn = i + 1;
                break;
            }
        }
        console.log("Clicking in : ", "tr:nth-child(" + rowIndex + ") td:nth-Child(" + headerInColumn + ") a")
        const clickonCell = await table.$("tr:nth-child(" + rowIndex + ") td:nth-Child(" + headerInColumn + ") a");
        await clickonCell.scrollIntoView();
        await browser.pause(1000)
        await clickonCell.click();
        await browser.pause(2000)
    }


}