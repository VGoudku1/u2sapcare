import * as EC from 'wdio-wait-for'
import { logStep } from "../utils/reporter";
import { ChainablePromiseElement } from 'webdriverio'
export class WebActions {

    /**
    * @param elementLocator - locator of the element from web
    * @returns webElement
    */
    async getWebElement(elementLocator: string) {
        return await browser.$(elementLocator);
    }

    /**
    * @param elementLocator - locator of the element from web
    * @returns array of webElement
    */
    async getAllWebElement(elementLocator: string) {
        return await browser.$$(elementLocator);
    }

    /**
    * @param elementLocator - locator of the element from web
    * @returns webElement where text matches
    */
    async getWebElementWithText(elementText: string, elements: Array<WebdriverIO.Element>) {
        let actualText: string = null;
        let fetchedElement: WebdriverIO.Element = null;
        let noOfElements: number = elements.length;
        console.log("No OF elements: ", noOfElements)
        if (noOfElements > 1) {
            for (let i: number = 0; i <= noOfElements - 1; i++) {
                actualText = await elements[i].getText();
                console.log("Matching Text: ", actualText.trim().toLowerCase(), "with ", elementText.toLowerCase(), " is: ", (actualText.trim().toLowerCase() == elementText.toLowerCase()))
                if (actualText.trim().toLowerCase() == elementText.toLowerCase()) {
                    fetchedElement = elements[i];
                    break;
                }
            }
        } else {
            actualText = await elements[0].getText();
            console.log("Only One Element in result", actualText)
            if (actualText.trim().toLowerCase() == elementText.toLowerCase()) {
                fetchedElement = elements[0];
            }
        }
        return fetchedElement;
    }



    /**
    * @param elementLocator - locator of the element to click
    * @returns click on a webelement
    */
    async clickElementWithText(elementTextToClick: string, elements: Array<WebdriverIO.Element>) {
        let actualText: string = null;
        let fetchedElement: WebdriverIO.Element = null;
        let noOfElements: number = elements.length;
        console.log("No OF elements: ", noOfElements)
        if (noOfElements > 1) {
            for (let i: number = 0; i <= noOfElements - 1; i++) {
                actualText = await elements[i].getText();
                console.log("Matching Text: ", actualText.trim().toLowerCase(), "with ", elementTextToClick.toLowerCase(), " is: ", (actualText.trim().toLowerCase() == elementTextToClick.toLowerCase()))
                if (actualText.trim().toLowerCase() == elementTextToClick.toLowerCase()) {
                    fetchedElement = elements[i];
                    await elements[i].scrollIntoView()
                    await elements[i].click();
                    break;
                }
            }
        } else {
            actualText = await elements[0].getText();
            console.log("Only One Element in result", actualText)
            if (actualText.trim().toLowerCase() == elementTextToClick.toLowerCase()) {
                fetchedElement = elements[0];
                await elements[0].click();
            }
        }
        logStep(`Clicked Element ${JSON.stringify(fetchedElement.selector)} with text: ` + elementTextToClick)
    }

    /**
    * @param elementLocator - locator of the element to click
    * @returns click on a webelement
    */
    async clickElement(element: ChainablePromiseElement<WebdriverIO.Element>) {
        await (await element).click();
        logStep(`Clicked on ${JSON.stringify(await element.selector)}`)
    }

    /**
    * @param elementLocator - locator of the element to click
    * @returns click on a webelement
    */
    async clickElementIfExists(element: ChainablePromiseElement<WebdriverIO.Element>) {
        const isElement = await this.isElementExists(element)
        if (isElement) {
            await element.click();
            logStep(`Clicked on existing element ${JSON.stringify(await element.selector)}`)
        }
    }

    /**
    * @param elementLocator - locator of the element to click
    * @returns click on a webelement
    */
    async clickElementByJScript(element: ChainablePromiseElement<WebdriverIO.Element>) {
        // await browser.execute("arguments[0].click();",await browser.$(`${JSON.stringify(await element.selector)}`))
        const ele = await browser.executeScript('document.querySelector("table[id*="ConfCellTable_TableHeader_fixedLeftTable"] tbody tr:nth-child(20) td[class*="first"]")', [])
        await ele.click();
        logStep(`Clicked By JScript on ${JSON.stringify(await element.selector)}`)
    }

    async setData(value: string, element: ChainablePromiseElement<WebdriverIO.Element>) {
        await element.setValue(value);
        logStep(`Entered '${value}' in ${JSON.stringify(await element.selector)}`)
    }

    async fetchText(element: ChainablePromiseElement<WebdriverIO.Element>) {
        const text = await element.getText();
        return text.trim();
    }

    async isElementDisplayed(element: ChainablePromiseElement<WebdriverIO.Element>) {
        logStep(`Element ${JSON.stringify(await element.selector)}is displayed: ${await element.isDisplayed()}`)
        return element.isDisplayed();
    }

    isElementExists(element: ChainablePromiseElement<WebdriverIO.Element>) {
        return element.isExisting();
    }

    async scrollIntoView(element: ChainablePromiseElement<WebdriverIO.Element>) {
        await element.scrollIntoView()
        logStep(`Element Scrolled Into View ${JSON.stringify(await element.selector)}`)
    }

    async selectByAttribute(attribute: string, value: string, element: ChainablePromiseElement<WebdriverIO.Element>) {
        await element.selectByAttribute(attribute, value);
        logStep(`Selected value '${value}' from ${JSON.stringify(await element.selector)} dropdown`)
    }

    async selectByText(text: string, element: ChainablePromiseElement<WebdriverIO.Element>) {
        await element.selectByVisibleText(text);
        logStep(`Selected text '${text}' from ${JSON.stringify(await element.selector)} dropdown`)
    }

    async selectItemInList(listItem: string, element: ChainablePromiseElement<WebdriverIO.Element>) {
        let actualText: string = null;
        console.log("Click Element: ", await (element.$('~ div a')))
        await this.clickElement(element.$('~ div a'));
        await browser.pause(1000);
        let listItems = element.$$("ul[role='listbox'] li a");
        let noOfElements: number = await listItems.length;
        console.log("No of elements in Listbox: ", noOfElements)
        if (noOfElements > 1) {
            for (let i: number = 0; i <= noOfElements - 1; i++) {
                actualText = await listItems[i].getText();
                if (actualText.trim().toLowerCase() == listItem.toLowerCase()) {
                    (await listItems[i]).click();
                    break;
                }
            }
        } else {
            actualText = await listItems[0].getText();
            console.log("Only One Element in List", actualText)
            if (actualText.trim().toLowerCase() == listItem.toLowerCase()) {
                (await listItems[0]).click();
            }
        }
        logStep(`Selected text '${listItem}' from ${JSON.stringify(await element.selector)} list`)
    }

}