import { Given, Then, When } from '@wdio/cucumber-framework';
import { ShopPlanStepsCode } from '../steps-code/shop-plan-code';

const shopPlan = new ShopPlanStepsCode();

When("I select the sap care plan {string}", async (plan: string) => {
    await shopPlan.selectPlan(plan, false)
    await browser.pause(2000);
    await shopPlan.clickAddToCartButton(plan);
});

When("I select sap care plan for device {string}", async (devicePlan: string) => {
    await shopPlan.selectPlan(devicePlan, false)
    await browser.pause(2000);
    await shopPlan.clickAddToCartButtonForDevicePlan(devicePlan);
});

When("I select the sap care plan {string} iFrame", async (plan: string) => {
    await shopPlan.selectPlan(plan, true)
    await browser.pause(2000);
    await shopPlan.clickAddToCartButton(plan)
});