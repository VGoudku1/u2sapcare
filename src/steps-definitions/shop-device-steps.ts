import { Given, Then, When } from '@wdio/cucumber-framework';
import { ShopDeviceStepsCode } from '../steps-code/shop-device-code';
import { ShopPlanStepsCode } from '../steps-code/shop-plan-code';

const shopDeviceSteps = new ShopDeviceStepsCode();
const shopPlan = new ShopPlanStepsCode();

When("I click on sap care button {string}", async (buttonText: string) => {
    await browser.pause(2000);
    await shopDeviceSteps.clickShopDeviceButton(buttonText);
    await browser.pause(2000);
});

When("I click iframe button {string}", async (buttonText: string) => {
    await shopDeviceSteps.clickiFrameButton(buttonText);
    await browser.pause(2000);
});

When("I select product type as {string}", async (product: string) => {
    await shopDeviceSteps.selectFromProductTypeDropdown(product)
    await browser.pause(2000);
});

When("I select device operating system type as {string}", async (osType: string) => {
    await shopDeviceSteps.selectFromOsTypeDropdown(osType)
    await browser.pause(2000);
});

When("I select device model with name as {string}", async (deviceName: string) => {
    await shopDeviceSteps.selectDevice(deviceName, false)
    await browser.pause(2000);
    await shopDeviceSteps.addToCartSelectedDevice(deviceName)
});