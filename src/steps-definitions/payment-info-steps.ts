import { Given, Then, When} from '@wdio/cucumber-framework';
import { CustomerInfoStepsCode } from '../steps-code/customer-info-code';
import { PaymentInfoStepsCode } from '../steps-code/payment-info-code';

const paymentPage = new PaymentInfoStepsCode();
const customerInfo = new CustomerInfoStepsCode();

When("I click footer button {string}", async (mobileNo:string) => {  
    await paymentPage.clickFooterBarButton(mobileNo)
});

When("I click edit payment button", async () => {  
    await paymentPage.clickEditPaymentButton()
});

When("I enter credit card payment details", async () => {  
    await paymentPage.selectPaymentMethod("Credit");
    await paymentPage.fillCreditCardDetails();
});

When("I click customer consent checkbox", async () => {  
    await paymentPage.clickConsentCheckBox();
});

When("I click consent checkbox to suspend line", async () => {  
    await paymentPage.clickSuspendConsentCheckBox();
});

When("Validate address on payment page", async () => {  
    await paymentPage.validateAddress();
    await customerInfo.acceptValidAddress();
});

When("I click on review T&C footer button", async () => {  
    await paymentPage.reviewTermsAndConditionsButton();
});

