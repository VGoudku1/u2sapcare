import { Given, Then, When } from '@wdio/cucumber-framework';
import { CancelLineStepsCode } from '../steps-code/cancel-line-code';



const cancelLine = new CancelLineStepsCode()

When("I select the reason to cancel {string}", async (reason:string) => {
    await cancelLine.selectReasonToCancel(reason);
});

When("I select cancel date as today", async () => {
    await cancelLine.selectTodayRadioButton();
});
