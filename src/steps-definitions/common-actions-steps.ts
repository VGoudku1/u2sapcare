import { Given, Then, When } from '@wdio/cucumber-framework';
import { BrowserUtils } from '../utils/browserUtils';

const browserUtils = new BrowserUtils

When("I click OK popup button", async () => {
    await browserUtils.popupButtonOk();
});

When("I Cancel the popup", async () => {
    
});