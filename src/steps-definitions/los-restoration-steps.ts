import { Given, Then, When } from '@wdio/cucumber-framework';
import { LosRestorationStepsCode } from '../steps-code/los-restoration-page-code';
import { SuspendLineStepsCode } from '../steps-code/suspend-line-code';


const losRestorationSteps = new LosRestorationStepsCode()

When("I select the reason to restore {string}", async (reason:string) => {
    await losRestorationSteps.selectReasonToRestore(reason);
});
