import { Given, Then, When } from '@wdio/cucumber-framework';
import { MsISDNPortStepsCode } from '../steps-code/msisdn-port-code';
import { Common } from '../utils/CommonEnum';

const msIsdnSteps = new MsISDNPortStepsCode();
const common = new Common();

When("I lookup mobile number as {string}", async (mobileNo: string) => {
    await msIsdnSteps.setDataInTextBox("Mobile No.", mobileNo)
});