import { Given, Then, When } from '@wdio/cucumber-framework';
import { ShoServicesStepsCode } from '../steps-code/shop-service-code';

const shopService = new ShoServicesStepsCode();

When("I add the service add-on plan {string}", async (serviceName: string) => {
    await browser.pause(2000);
    await shopService.clickAddToCartButtonForAddOnService(serviceName);
});