import { Given, Then, When} from '@wdio/cucumber-framework';
import { ShopDevicePageObjects } from '../pages/shop-devices-page';
import { ActivationStepsCode } from '../steps-code/activation-page-code';
import { ShopDeviceStepsCode } from '../steps-code/shop-device-code';
import { WaitUtils } from '../utils/WaitUtils';
import { WebActions } from '../utils/WebActions';

const activationSteps = new ActivationStepsCode();
const shopDeviceSteps = new ShopDeviceStepsCode();
const shopDevice = new ShopDevicePageObjects();
const webActions = new WebActions();
const wait = new WaitUtils();

When("I enter area zipcode {string}", async (zipCode: string) => {  
    await activationSteps.enterZipCode(zipCode);
    await browser.pause(2000);
});

When("I enter IMEI code {string}", async (imeiCode:string) => {
    await activationSteps.enterIMEICode(imeiCode);
    await browser.pause(3000);
});

Given("I validate the details entered", async () => {
    await shopDeviceSteps.clickShopDeviceButton('VALIDATE');
    await browser.pause(8000);
    await activationSteps.messageContainer();
});

Then("I enter SIM number {string}", async (simNo:string) => {
    await activationSteps.enterSimNo(simNo);
    await browser.pause(3000);
});

When("I add the SIM plan as {string} to cart", async (simType: string) => {
    await activationSteps.clickAddToCartSIM(simType)
    await browser.pause(2000);
});