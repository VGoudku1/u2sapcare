import { Given, Then, When } from '@wdio/cucumber-framework';
import { NumberLiteralType } from 'typescript';
import login from '../steps-code/login-page-code';
import { WaitUtils } from '../utils/WaitUtils';
const wait = new WaitUtils();


Given(/^User login as TEST_TEAM into sap care portal$/, async () => {
  await login.openApplication()
  await browser.pause(2000);
  await login.loginPerSession();
  // await login.enterUserName()
  // await browser.pause(2000);
  // await login.clickOnSingIn()
  // await browser.pause(1000);
  // await login.killExistingSession(true);
  // await login.sessionContinueButton()
  // await browser.pause(15000);
});

Given(/^User wait for (\d+) seconds$/, async (seconds: number) => {
  const secs = seconds * 1000
  await browser.pause(secs);
});

Given(/^User wait (\d+) secs for spin wheel to disappear$/, async (seconds: number) => {
  await wait.waitForSpinWheel(seconds * 1000);
});

