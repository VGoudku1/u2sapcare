import { Given, Then, When } from '@wdio/cucumber-framework';
import { MsISDNPortStepsCode } from '../steps-code/msisdn-port-code';

const msIsdnSteps = new MsISDNPortStepsCode();

When("I enter NPA mobile number as {string}", async (mobileNo: string) => {
    await msIsdnSteps.enterSearchNPAMobileNo(mobileNo)
});

When("I click on first mobile no in results", async () => {
    await msIsdnSteps.clickFirstPhoneNo();
});

When("I select primary number as first row", async () => {
    await msIsdnSteps.selectPrimaryNumber()
});

When("I select secondary number as second row", async () => {
    await msIsdnSteps.selectSecondaryNumber()
});