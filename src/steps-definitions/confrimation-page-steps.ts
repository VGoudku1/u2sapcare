import { Given, Then, When } from '@wdio/cucumber-framework';
import { ShopDevicePageObjects } from '../pages/shop-devices-page';
import { ActivationStepsCode } from '../steps-code/activation-page-code';
import { ConfirmationStepsCode } from '../steps-code/confirmation-page-code';
import { ShopDeviceStepsCode } from '../steps-code/shop-device-code';
import { Common } from '../utils/CommonEnum';
import { WaitUtils } from '../utils/WaitUtils';
import { WebActions } from '../utils/WebActions';
import { MsISDNPortStepsCode } from '../steps-code/msisdn-port-code';
import CucumberJsJsonReporter from 'wdio-cucumberjs-json-reporter';
import { getValue, setValue } from '@wdio/shared-store-service';

const msIsdnSteps = new MsISDNPortStepsCode();

const confirmationStepsCode = new ConfirmationStepsCode();
const shopDeviceSteps = new ShopDeviceStepsCode();
const shopDevice = new ShopDevicePageObjects();
const webActions = new WebActions();
const wait = new WaitUtils();
const common = new Common();
let classOrderNumber: string = null;




When("Verify order is submited and order number is generated", async () => {
    await confirmationStepsCode.verifyOrderGenerated();
    classOrderNumber = await confirmationStepsCode.captureOrderNumber();
    // classOrderNumber = "000999";
    await confirmationStepsCode.writeOrderNumberToFile(classOrderNumber);
    // await browser.sharedStore.set("key", orderObject)
    // await Promise.resolve()
    CucumberJsJsonReporter.attach("The Order Number Generated: " + classOrderNumber)
});

When("I lookup order number as {string}", async (orderNumber: string) => {
    const enterOrder: string = common.load(1);
    const orderNumberFromFile = await msIsdnSteps.readOrderNumberFromFile()
    await msIsdnSteps.setDataInTextBox("Order/Cart ID:", orderNumberFromFile)
});