import { Given, Then, When } from '@wdio/cucumber-framework';
import { ResumeLineStepsCode } from '../steps-code/resume-line-code';

const resumeLine = new ResumeLineStepsCode()

When("I select the reason to resume {string}", async (reason:string) => {
    await resumeLine.selectReasonToResume(reason);
});
