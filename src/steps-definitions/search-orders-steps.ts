import { Given, Then, When } from '@wdio/cucumber-framework';
import { SearchOrdersStepsCode } from '../steps-code/search-orders-code';

const searchOrders = new SearchOrdersStepsCode()

When("I click cutomer ID of order number {string}", async (orderNumber: string) => {
    await searchOrders.clickCustomerID(orderNumber);
    await browser.pause(2000);
});