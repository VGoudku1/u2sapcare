import { Given, Then, When } from '@wdio/cucumber-framework';
import { SuspendLineStepsCode } from '../steps-code/suspend-line-code';


const suspendLine = new SuspendLineStepsCode()

When("I select the reason to suspend {string}", async (reason:string) => {
    await suspendLine.selectReasonToSuspend(reason);
});
