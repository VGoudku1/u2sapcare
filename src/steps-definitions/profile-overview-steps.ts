import { Given, Then, When } from '@wdio/cucumber-framework';
import { ProfileOverviewStepsCode } from '../steps-code/profile-overview-code';


const profileOverview = new ProfileOverviewStepsCode()

When("I click financial account number", async () => {
    await profileOverview.clickFinancialAccount()
    await browser.pause(2000);
});

When("I select the default plan checkbox", async () => {
    await profileOverview.selectPlanCheckBox();
    await browser.pause(2000);
});

When("I select the plan to suspend {string}", async (planName: string) => {
    await profileOverview.selectPlanToSuspend(planName);
    await browser.pause(2000);
});

When("I select more menu option {string}", async (optionName: string) => {
    await profileOverview.selectMoreMenuOption(optionName);

});