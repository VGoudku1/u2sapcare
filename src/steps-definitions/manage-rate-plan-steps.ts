import { Given, Then, When } from '@wdio/cucumber-framework';

import { ManageRatePlanStepsCode } from '../steps-code/manage-rate-plan';


const manageRatePlan = new ManageRatePlanStepsCode()

When("I select the sap care plan {string} to replace", async (plan: string) => {
    await manageRatePlan.selectPlanToReplace(plan, true)
    await browser.pause(2000);
    await manageRatePlan.clickReplaceButton(plan);
});

When("I select existing plan to replace {string}", async (planName:string) => {
    await manageRatePlan.selectExistingPlanRow(planName)
});
