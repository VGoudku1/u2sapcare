@sap
Feature: Activate Plan and BYOS test case
    As a rebelian user i want to activate $15 plan for prepaid user
    @local
    Scenario Outline: 1: Activate <PlanName> with new SIM
        Given User login as TEST_TEAM into sap care portal
        When I select option "Shop/Activation" from left side menu
        And I click on sap care button "BYOD"
        And I enter area zipcode "<ZipCode>"
        And I enter IMEI code "<IMEI>"
        And I enter SIM number "<SIMNo>"
        And I validate the details entered
        And I select the sap care plan "<PlanName>" iFrame
        And I click on sap care button "Review Cart"
        And I click review cart button "Next"
        And I enter customer first name as "TestName"
        And I enter customer last name as "TestLastName"
        And I enter customer PIN as "123456"
        And I validate the address details entered
        And I click iframe button "Next"
        And User wait for 5 seconds
        And I enter NPA mobile number as "720"
        And I click on sap care button "Search"
        And I click on first mobile no in results
        And I click on sap care button "Reserve"
        And I click on sap care button "Next"
        And User wait for 5 seconds
        And I click footer button "Review T&C"
        And I click edit payment button
        And I enter credit card payment details
        And I enter address line1 as "717 23rd Ave"
        And I click footer button "Review T&C"
        And Validate address on payment page
        And User wait for 4 seconds
        And I click customer consent checkbox
        And I click footer button "Submit"
        And User wait for 15 seconds
        And Verify order is submited and order number is generated

        Examples:
            | ZipCode | IMEI            | SIMNo               | SIMType                    | PlanName           |
            | 98122   | 357217070874259 | 8901260243764741429 | T-Mobile Prepaid Unlimited | $25 Connect by TMO |
    # | 98122   | 355619081274498 | 8901260998749599735 | T-Mobile Prepaid Unlimited | $15 Connect by TMO |

    Scenario: 2: Shop Sim and Activate $15 Plan
        Given User login as TEST_TEAM into sap care portal
        When I select option "Shop/Activation" from left side menu
        And I click on sap care button "BYOD"
        And I enter area zipcode "<ZipCode>"
        And I enter IMEI code "<IMEI>"
        And I click on sap care button "Shop SIM"
        And I add the SIM plan as "<SIMType>" to cart
        And I select the sap care plan "<PlanName>"
        And I click on sap care button "Review Cart"
        And User wait for 5 seconds
        And I click review cart button "Next"
        And User wait for 5 seconds
        And I enter customer first name as "TestName"
        And I enter customer last name as "TestLastName"
        And I enter customer PIN as "123456"
        And I validate the address details entered
        And I click iframe button "Next"
        And User wait for 5 seconds
        And I enter NPA mobile number as "720"
        And I click on sap care button "Search"
        And I click on first mobile no in results
        And I click on sap care button "Reserve"
        And I click on sap care button "Next"
        And User wait for 5 seconds
        And I enter address line1 as "717 23rd Ave"
        And I enter shipping email address as "sampleemail@email.com"
        And I validate the address details entered
        And User wait for 10 seconds
        And I click iframe button "Next"
        And User wait for 15 seconds
        And I click footer button "Review T&C"
        And I click edit payment button
        And I enter credit card payment details
        And I enter address line1 as "717 23rd Ave"
        And I click footer button "Review T&C"
        And Validate address on payment page
        And User wait for 4 seconds
        And I click customer consent checkbox
        And I click footer button "Submit"
        And User wait for 15 seconds
        And Verify order is submited and order number is generated


        Examples:
            | ZipCode | IMEI            | SIMType                 | PlanName           |
            | 98122   | 352557072747812 | T-Mobile TripleSIM card | $25 Connect by TMO |
