
Feature: Changing the existing activated rate plan
    As a sapcare user, I want to open the existing acivated order
    So that I can change or update the rate plan.

    Scenario Outline: 1: Change Rate Plan of exesting activated SIM
        Given User login as TEST_TEAM into sap care portal
        When I select option "Order Management" from left side menu
        And I lookup order number as "<OrderNumber>"
        And I click on sap care button "Search"
        And I click cutomer ID of order number "<OrderNumber>"
        And I click on sap care button "Bypass"
        And User wait for 5 seconds
        And I click financial account number
        And I select the default plan checkbox
        And I click on sap care button "Change Rate Plan"
        And I click OK popup button
        And I select existing plan to replace "$15"
        And I select the sap care plan "<ReplacePlanName>" to replace
        And I click on sap care button "Review Cart"
        And User wait for 4 seconds
        And I click on sap care button "Next"
        And I click footer button "Review T&C"
        And I click edit payment button
        And I enter credit card payment details
        And I enter address line1 as "717 23rd Ave"
        And I click footer button "Review T&C"
        And Validate address on payment page
        And User wait for 4 seconds
        And I click customer consent checkbox
        And I click footer button "Submit"
        And User wait for 15 seconds
        And Verify order is submited and order number is generated

        Examples:
            | OrderNumber    | ReplacePlanName                                       |
            | CTLMU0PG224412 | $50 Simply Prepaid UNL. Talk, Text & 10GB 5G & 4G LTE |
