
Feature: Activate Plan and BYOS test case
    As a rebelian user i want to activate plan for prepaid user and also add additional plans or services


    Scenario Outline: 1: Activate <PlanName> with new SIM
        Given User login as TEST_TEAM into sap care portal
        When I select option "Shop/Activation" from left side menu
        And I click on sap care button "BYOD"
        And I enter area zipcode "<ZipCode>"
        And I enter IMEI code "<IMEI>"
        And I enter SIM number "<SIMNo>"
        And I validate the details entered
        And I select the sap care plan "<PlanName>" iFrame
        And I click on sap care button "Review Cart"
        And I click on sap care button "More Options"
        And I select more options "Shop Device and Plans"
        And I click on sap care button "BYOD"
        And I enter area zipcode "<ZipCode>"
        And I enter IMEI code "<IMEI>"
        #2nd SIM
        And I enter SIM number "8901260998749601119"
        And I validate the details entered
        And I select the sap care plan "$25 Connect by TMO" iFrame
        And I click on sap care button "Review Cart"
        And I click review cart button "Next"
        And I enter customer first name as "TestName"
        And I enter customer last name as "TestLastName"
        And I enter customer PIN as "123456"
        And I validate the address details entered
        And I click iframe button "Next"
        And User wait for 5 seconds
        And I enter NPA mobile number as "720"
        And I click on sap care button "Search"
        And I click on first mobile no in results
        And I click on sap care button "Reserve"
        And I click on sap care button "Next"
        And User wait for 5 seconds
        And I click footer button "Review T&C"
        And I click edit payment button
        And I enter credit card payment details
        And I enter address line1 as "717 23rd Ave"
        And I click footer button "Review T&C"
        And Validate address on payment page
        And User wait for 4 seconds
        And I click customer consent checkbox
        And I click footer button "Submit"
        And User wait for 15 seconds
        And Verify order is submited and order number is generated


        Examples:
            | ZipCode | IMEI            | SIMNo               | SIMType                    | PlanName           |
            | 98122   | 355619081274498 | 8901260998749601143 | T-Mobile Prepaid Unlimited | $15 Connect by TMO |

    Scenario Outline: 2: Multiline Device Activate <PlanName> with new SIM
        Given User login as TEST_TEAM into sap care portal
        When I select option "Shop/Activation" from left side menu
        And I enter area zipcode "<ZipCode>"
        And I select product type as "<ProductType>"
        And I select device operating system type as "<DeviceOsType>"
        And I click on sap care button "Search"
        And User wait for 5 seconds
        And I select device model with name as "<DeviceModel>"
        And I select sap care plan for device "<PlanName>"
        And I click on sap care button "Review Cart"
        And I click review cart button "Next"
        And I enter customer first name as "TestName"
        And I enter customer last name as "TestLastName"
        And I enter customer PIN as "123456"
        And I validate the address details entered
        And I click iframe button "Next"
        And User wait for 5 seconds
        And I enter NPA mobile number as "720"
        And I click on sap care button "Search"
        And I click on first mobile no in results
        And I click on sap care button "Reserve"
        And I click on sap care button "Next"
        And User wait for 5 seconds
        And I enter address line1 as "717 23rd Ave"
        And I enter shipping email address as "sampleemail@email.com"
        And I validate the address details entered
        And User wait for 10 seconds
        And I click iframe button "Next"
        And I click footer button "Review T&C"
        And I click edit payment button
        And I enter credit card payment details
        And I enter address line1 as "717 23rd Ave"
        And I click footer button "Review T&C"
        And Validate address on payment page
        And User wait for 4 seconds
        And I click customer consent checkbox
        And I click footer button "Submit"
        And User wait for 15 seconds
        And Verify order is submited and order number is generated

        Examples:
            | ZipCode | ProductType | DeviceOsType | DeviceModel    | PlanName           |
            | 98122   | Handset     | iOS          | iPhone 14 Plus | $15 Connect by TMO |


    Scenario Outline: 3: Multiline Activate <PLanName> with new SIM
        Given User login as TEST_TEAM into sap care portal
        When I select option "Shop/Activation" from left side menu
        And I click on sap care button "BYOD"
        And I enter area zipcode "<ZipCode>"
        And I click on sap care button "Shop SIM"
        And I add the SIM plan as "<SIMType>" to cart
        And I select the sap care plan "<PlanName>"
        And I click on sap care button "Review Cart"
        And I click on sap care button "More Options"
        And I select more options "Shop SIM"
        And I add the SIM plan as "<SIMType2>" to cart
        And I select the sap care plan "$25 Connect by TMO"
        And I click on sap care button "Review Cart"
        And I click review cart button "Next"
        And I enter customer first name as "TestName"
        And I enter customer last name as "TestLastName"
        And I enter customer PIN as "123456"
        And I validate the address details entered
        And I click iframe button "Next"
        And User wait for 5 seconds

        And I select primary number as first row
        And I enter NPA mobile number as "720"
        And I click on sap care button "Search"
        And User wait for 5 seconds
        And I click on first mobile no in results
        And I click on sap care button "Reserve"

        And I select secondary number as second row
        And I enter NPA mobile number as "720"
        And I click on sap care button "Search"
        And User wait for 5 seconds
        And I click on first mobile no in results
        And I click on sap care button "Reserve"

        And I click on sap care button "Next"
        And User wait for 5 seconds
        And I enter address line1 as "717 23rd Ave"
        And I enter shipping email address as "sampleemail@email.com"
        And I validate the address details entered
        And User wait for 10 seconds
        And I click iframe button "Next"
        And I click footer button "Review T&C"
        And I click edit payment button
        And I enter credit card payment details
        And I enter address line1 as "717 23rd Ave"
        And I click footer button "Review T&C"
        And Validate address on payment page
        And User wait for 4 seconds
        And I click customer consent checkbox
        And I click footer button "Submit"
        And User wait for 15 seconds
        And Verify order is submited and order number is generated

        # And I click on "Review Cart" button
        # And I click on  "Next" button
        # And I enter customer first name as "TestName"
        # And I enter customer last name as "TestLastName"
        # And I enter customer PIN as "123456"
        # And I validate the address details entered
        # And I click iframe button "Next"
        # And I select Primary line
        # And I enter NPA mobile number as "760"
        # And I click on sap care button "Search"
        # And I click on first mobile no in results
        # And I click on sap care button "Reserve"
        # And I select secondry line
        # And I enter NPA mobile number as "425"
        # And I click on sap care button "Search"
        # And I click on first mobile no in results
        # And I click on sap care button "Reserve"
        # And I click on sap care button "Next"
        # And I Navigated to Fulfillment Detailsv Pag
        # And I enter User Email in "Shipping Notification Email field"
        # And I validate the shipping address details entered
        # And User wait for 5 seconds
        # And I click footer button "Review T&C"
        # And I click edit payment button
        # And I enter credit card payment details
        # And I enter address line1 as "717 23rd Ave"
        # And I click footer button "Review T&C"
        # And Validate address on payment page
        # And User wait for 4 seconds
        # And I click customer consent checkbox
        # And I click footer button "Submit"
        # And User wait for 15 seconds
        # And Verify order is submited and order number is generated

        Examples:
            | ZipCode | SIMType                 | PlanName           | SIMType2                       |
            | 98122   | T-Mobile TripleSIM card | $15 Connect by TMO | Prepaid 3-in-1 SIM Starter Kit |