@sap
Feature: Suspend activated plans
    As a rebelian user i want to suspend the activated plans for prepaid user

    Scenario: 1: Lookup phone number and suspend plan
        Given User login as TEST_TEAM into sap care portal
        When I select option "Lookup" from left side menu
        # And I lookup mobile number as "7206552717"
        # And I click on sap care button "Search"

        Examples:
            | ZipCode | IMEI            | SIMNo               | SIMType                    | PLanName           |
            | 98122   | 355619081274498 | 8901260998780464682 | T-Mobile Prepaid Unlimited | $15 Connect by TMO |
    @local
    Scenario: 2: Search order number and suspend plan
        Given User login as TEST_TEAM into sap care portal
        When I select option "Order Management" from left side menu
        And I lookup order number as "<OrderNumber>"
        And I click on sap care button "Search"
        And I click cutomer ID of order number "<OrderNumber>"
        And I click on sap care button "Bypass"
        And I click financial account number
        And I select the default plan checkbox
        And I click on sap care button "Suspend"
        And I select the reason to suspend "Equipment Lost or Stolen"
        And I click on sap care button "Review"
        And User wait for 2 seconds
        And I click on sap care button "Next"
        And User wait for 4 seconds
        And I click on sap care button "Next"
        And I click footer button "Review T&C"
        And User wait for 4 seconds
        And I click consent checkbox to suspend line
        And I click footer button "Submit"
        And User wait for 10 seconds
        And Verify order is submited and order number is generated

        #CTLN60MK222897
        Examples:
            | OrderNumber    |
            | CTLJB0LIS42889 |
    # | CTLMA0QQY29141 |

    Scenario: 3: Restore suspended plan
        Given User login as TEST_TEAM into sap care portal
        When I select option "Order Management" from left side menu
        And I lookup order number as "<OrderNumber>"
        And I click on sap care button "Search"
        And I click cutomer ID of order number "<OrderNumber>"
        And I click on sap care button "Bypass"
        And I click financial account number
        And I select the default plan checkbox
        And I select more menu option "Restore"
        And I select the default plan checkbox
        And I click on sap care button "Next"
        And I select the reason to restore "Customer Request Resume"
        And I click on sap care button "Apply to All Lines"
        And I click on sap care button "Add To Cart"
        And User wait for 2 seconds
        And I click on sap care button "Review Cart"
        And I click on sap care button "Next"
        And User wait for 4 seconds
        And I click on sap care button "Next"
        And I click footer button "Review T&C"
        And User wait for 4 seconds
        And I click consent checkbox to suspend line
        And I click footer button "Submit"
        And User wait for 10 seconds
        And Verify order is submited and order number is generated

        #CTLN60MK222897
        Examples:
            | OrderNumber    |
            | CTLMA0RD829143 |
    # | CTLOW0ZAC10359 |

    Scenario: 4: Cancel the restored plan
        Given User login as TEST_TEAM into sap care portal
        When I select option "Order Management" from left side menu
        And I lookup order number as "<OrderNumber>"
        And I click on sap care button "Search"
        And I click cutomer ID of order number "<OrderNumber>"
        And I click on sap care button "Bypass"
        And I click financial account number
        And I select the default plan checkbox
        And I select more menu option "Cancel"
        And I select the reason to cancel "Customer Request Cancel"
        And I select cancel date as today
        And I click on sap care button "Review"
        And I click on sap care button "Next"
        And I click on sap care button "Next"
        And I click footer button "Review T&C"
        And User wait for 4 seconds
        And I click consent checkbox to suspend line
        And I click footer button "Submit"
        And User wait for 10 seconds
        And Verify order is submited and order number is generated

        #CTLN60MK222897
        Examples:
            | OrderNumber    |
            | CTLMA0RK629146 |

    Scenario: 5: Resume the cancelled plan
        Given User login as TEST_TEAM into sap care portal
        When I select option "Order Management" from left side menu
        And I lookup order number as "<OrderNumber>"
        And I click on sap care button "Search"
        And I click cutomer ID of order number "<OrderNumber>"
        And I click on sap care button "Bypass"
        And I click financial account number
        And I select the default plan checkbox
        And I select more menu option "Resume"
        And I select the reason to resume "Customer Request Resume"
        And I click on sap care button "Apply Date"
        And I select the default plan checkbox
        And I click on sap care button "Next"
        And I click on sap care button "Checkout"
        And I click on sap care button "Next"
        And I click on sap care button "Next"
        And I click footer button "Review T&C"
        And User wait for 4 seconds
        And I click consent checkbox to suspend line
        And I click footer button "Submit"
        And User wait for 10 seconds
        And Verify order is submited and order number is generated

        #CTLN60MK222897
        Examples:
            | OrderNumber    |
            | CTLMF10UP28268 |